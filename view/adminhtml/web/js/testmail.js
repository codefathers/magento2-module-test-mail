
define([
    "jquery",
    "Magento_Ui/js/modal/alert",
    "mage/translate",
    "jquery/ui"
], function ($, alert, $t) {
    "use strict";

    $.widget('cf.testmail', {
        options: {
            ajaxUrl: '',
            elemScope: 'system_smtp_'
        },
        _create: function () {
            var self = this;
            this.getElement('send_testmail').click(function (ev) {
                self.handleSendButtonClick(ev);
            });
        },
        getElement: function (name) {
            var scope = '#'+this.options.elemScope+name;
            return $(scope);
        },
        getValue: function (name) {
            return this.getElement(name).val();
        },
        getValues: function () {
            return {
                'action': 'send-test-mail',
                'testmail_to': this.getValue('testmail_to'),
            }
        },
        handleSendButtonClick: function (ev) {
            ev.preventDefault();
            var data = this.getValues();
            data.action='send-test-mail';
            $.ajax({
                type: "POST",
                url: this.options.ajaxUrl,
                data: data,
                dataType: 'json',
                showLoader: true,
                success: function (result) {
                    alert({
                        title: (result.result == 'success') ? $t('Success') : $t('Error'),
                        content: result.message
                    });
                }
            });
        }
    });

    return $.cf.testmail;
});
