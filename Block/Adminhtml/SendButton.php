<?php

namespace Cf\TestMail\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;


/**
 * Class SendButton
 * @package Cf\TestMail\Block\Adminhtml
 */
class SendButton extends Field
{
    /**
     * @var string
     */
    protected $_template = 'send-button.phtml';


    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope();
        return parent::render($element);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $originalData = $element->getOriginalData();
        $this->addData(
            [
                'button_label' => $originalData['button_label'],
                'button_url'   => $this->getUrl($originalData['button_url'], ['_current' => true]),
                'html_id'      => $element->getHtmlId(),
            ]
        );
        return $this->_toHtml();
    }
}
