<?php

namespace Cf\TestMail\Controller\Adminhtml\Testmail;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\ValidatorException;
use Magento\Email\Model\Template\SenderResolver;
use Magento\Framework\Mail\Template\TransportBuilder;
use Psr\Log\LoggerInterface;

/**
 * Class Send
 */
class Send extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    /*const ADMIN_RESOURCE = 'Magento_Backend::admin';*/

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var SenderResolver
     */
    protected $senderResolver;


    /**
     * Send constructor.
     * @param Context $context
     * @param LoggerInterface $logger
     * @param TransportBuilder $transportBuilder
     * @param SenderResolver $senderResolver
     */
    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Email\Model\Template\SenderResolver $senderResolver
    )
    {
        $this->logger = $logger;
        $this->transportBuilder = $transportBuilder;
        $this->senderResolver = $senderResolver;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $result = array(
            'result' => '',
            'message' => ''
        );
        $request = $this->getRequest();
        try {
            if ($request->getParam('action') !== 'send-test-mail') {
                throw new ValidatorException(__('Invalid Request'));
            }

            $scopeId = (int) $request->getParam('scope_id');

            $from = $this->senderResolver->resolve(
                'sales',
                $scopeId);

            $toId = $request->getParam('testmail_to');
            if (!$toId) {
                throw new ValidatorException(__('Please select an email contact of your store'));
            }
            $toData = $this->senderResolver->resolve(
                $toId,
                $scopeId);
            $toEmail = (isset($toData['email'])) ? $toData['email'] : '';

            if (!$toEmail || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                throw new ValidatorException(__('Invalid Receiver Email'));
            }

            $this->transportBuilder
                ->setTemplateIdentifier('sales_email_test_mail_template')
                ->setTemplateOptions([
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ])
                ->setTemplateVars([])
                ->setFrom($from)
                ->addTo($toEmail)
                ->getTransport()
                ->sendMessage();

            $result['result'] = 'success';
            $result['message'] = __('Mail successfully sent to \'%1\' - check your Mailbox.', $toEmail);


        } catch (\Exception $e) {
            $result['result'] = 'error';
            $result['message'] = __('Error while sending your mail: %1', $e->getMessage());
            $this->logger->critical($e);
        }
        return $this->getResponse()->representJson(json_encode($result));
    }


}
